package dwfe.test;

import dwfe.config.DwfeConfigProperties;
import dwfe.test.config.*;
import dwfe.util.DwfeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;

import static dwfe.test.config.DwfeTestAuthType.REFRESH;
import static dwfe.test.config.DwfeTestTypeResourceAccessing.BAD_ACCESS_TOKEN;
import static dwfe.test.config.DwfeTestTypeResourceAccessing.USUAL;
import static dwfe.test.config.DwfeTestVariablesForAuthAccessTest.AUTHORITY_to_AUTHORITY_STATUS;
import static dwfe.test.config.DwfeTestVariablesForAuthAccessTest.AUTHORITY_to_AUTHORITY_STATUS_BAD_ACCESS_TOKEN;
import static dwfe.util.DwfeUtil.getJsonFromObj;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Component
public class DwfeTestUtil
{
  private static final Logger log = LoggerFactory.getLogger(DwfeTestUtil.class);

  private final DwfeConfigProperties propDwfe;
  private final DwfeTestConfigProperties propDwfeTest;
  private final DwfeUtil utilDwfe;
  private final RestTemplateBuilder rtb;
  private final DwfeTestVariablesForAuthAccessTest VARS_FOR_AUTH_TESTS;

  private DwfeTestUtil(DwfeConfigProperties propDwfe,
                       DwfeTestConfigProperties propDwfeTest,
                       DwfeUtil utilDwfe,
                       RestTemplateBuilder restTemplateBuilder,
                       DwfeTestVariablesForAuthAccessTest varsForAuthTest)
  {
    this.propDwfe = propDwfe;
    this.propDwfeTest = propDwfeTest;
    this.utilDwfe = utilDwfe;
    this.rtb = restTemplateBuilder;
    this.VARS_FOR_AUTH_TESTS = varsForAuthTest;
  }


  //-------------------------------------------------------
  // Exchange Core
  //

  public Map<String, Object> performRequest(RestTemplate restTemplate, RequestEntity<?> req, int expectedStatus)
  {
    ResponseEntity<?> resp;
    Map<String, Object> body = null;
    var actualStatusCode = -1;
    var url = req.getUrl().toString();

    log.info("= authorization: {}", req.getHeaders().get("Authorization"));
    log.info("-> " + url);
    try
    {
      if (!url.contains("http://localhost"))
      {
        restTemplate.setRequestFactory(utilDwfe.getRibbonClientHttpRequestFactory());
      }
      resp = restTemplate.exchange(req, new ParameterizedTypeReference<Map<String, Object>>()
      {
      });
      actualStatusCode = resp.getStatusCodeValue();

      body = (Map<String, Object>) resp.getBody();
      log.info("= expected: {}", expectedStatus);
      log.info("<- {} {}\n", actualStatusCode, body);
    }
    catch (Throwable e)
    {
      if (e instanceof HttpClientErrorException)
      {
        HttpClientErrorException httpError = (HttpClientErrorException) e;
        actualStatusCode = httpError.getRawStatusCode();
        log.error("<- {} {} {}\n",
                actualStatusCode,
                httpError.getStatusCode().getReasonPhrase(),
                httpError.getResponseBodyAsString());
      }
      else
        e.printStackTrace();
    }
    assertEquals(expectedStatus, actualStatusCode);
    return body;
  }

  public RequestEntity<?> generateGETrequest(String url, String access_token)
  {
    var reqBuilder = RequestEntity.get(URI.create(url));
    if (access_token != null)
      reqBuilder.header("Authorization", "Bearer " + access_token);
    return reqBuilder.build();
  }

  public RequestEntity<?> generatePOSTrequest(String url, String access_token, Map<String, Object> map)
  {
    var reqBuilder = RequestEntity
            .post(URI.create(url))
            .contentType(MediaType.APPLICATION_JSON_UTF8);
    if (access_token != null)
      reqBuilder.header("Authorization", "Bearer " + access_token);

    var body = getJsonFromObj(map);
    log.info("= body: {}", body);

    return reqBuilder.body(body);
  }

  public Map<String, Object> responseAfterPOSTrequest(String url, String access_token, Map<String, Object> map, int expectedStatus)
  {
    var req = generatePOSTrequest(url, access_token, map);
    return performRequest(rtb.build(), req, expectedStatus);
  }

  public Map<String, Object> responseAfterGETrequest(String url, String access_token, int expectedStatus)
  {
    var req = generateGETrequest(url, access_token);
    return performRequest(rtb.build(), req, expectedStatus);
  }


  //-------------------------------------------------------
  // Exchange Processes
  //

  public Map<String, Object> tokenProcess(DwfeTestAuthType signInType, DwfeTestAuth auth, int expectedStatus)
  {
    var nevisUri = propDwfe.getService().getGatewayUrl() + propDwfe.getService().getNevis().getGatewayPath();
    String url;
    if (REFRESH == signInType)
    {
      url = String.format(nevisUri + propDwfeTest.getResource().getRefreshToken()
              + "?grant_type=refresh_token&refresh_token=%s", auth.refresh_token);

      log.info("token refreshing");
      log.info("= refresh token: {}", auth.refresh_token);
    }
    else
    {
      url = String.format(nevisUri + propDwfeTest.getResource().getSignIn()
                      + "?grant_type=password&username=%1$s&password=%2$s%3$s",
              URLEncoder.encode(auth.username, StandardCharsets.UTF_8),
              URLEncoder.encode(auth.password, StandardCharsets.UTF_8),
              auth.usernameType == null ? "" : "&usernameType=" + auth.usernameType
      );

      log.info("signing in");
      log.info("= auth credentials:  {}:{}; usernameType={}", auth.username, auth.password, auth.usernameType);
    }
    assertNotNull(url);

    RequestEntity<?> req = generatePOSTrequest(url, null, null);

    var client = auth.client;
    log.info("= client credentials:  {}:{}", client.clientname, client.clientpass);

    var restTemplate = rtb.basicAuthorization(client.clientname, client.clientpass).build();
    var body = performRequest(restTemplate, req, expectedStatus);

    if (expectedStatus == 200)
    {
      var access_token = (String) body.get("access_token");
      var refresh_token = (String) body.get("refresh_token");

      assertThat(access_token.length(), greaterThan(0));
      assertThat(refresh_token.length(), greaterThan(0));
      assertThat((int) body.get("expires_in"),
              is(both(greaterThan(client.minTokenExpirationTime)).and(lessThanOrEqualTo(client.maxTokenExpirationTime))));

      auth.access_token = access_token;
      auth.refresh_token = refresh_token;
    }
    return body;
  }

  public void fullAuthProcess(DwfeTestAuth auth)
  {
    //1,2,3
    resourceAccessing_with_changeToken_Process(auth);

    //4. Sign Out
    signOutProcess(auth, null);

    //5. Resource accessing
    resourceAccessingProcess(auth.access_token, auth.levelAuthority, BAD_ACCESS_TOKEN);

    //6. Change Token
    tokenProcess(REFRESH, auth, 400);
  }

  private void resourceAccessing_with_changeToken_Process(DwfeTestAuth auth)
  {
    //1. Resource accessing
    resourceAccessingProcess(auth.access_token, auth.levelAuthority, USUAL);

    //2. Change Token
    var old_access_token = auth.access_token;
    var old_refresh_token = auth.refresh_token;
    var expectedStatus = auth.client.clientname.equals(propDwfeTest.getOauth2ClientTrusted().getId()) ? 200 : 401;
    tokenProcess(REFRESH, auth, expectedStatus);
    if (expectedStatus == 200)
    {
      assertNotEquals(old_access_token, auth.access_token);
      assertEquals(old_refresh_token, auth.refresh_token);

      //3. Resource accessing: old/new token
      resourceAccessingProcess(old_access_token, auth.levelAuthority, BAD_ACCESS_TOKEN);
      resourceAccessingProcess(auth.access_token, auth.levelAuthority, USUAL);
    }
    else
    {
      resourceAccessingProcess(auth.access_token, auth.levelAuthority, BAD_ACCESS_TOKEN);
    }
  }

  public void resourceAccessingProcess(String access_token, DwfeTestLevelAuthority authorityLevel, DwfeTestTypeResourceAccessing resourceAccessingType)
  {
    Map<DwfeTestLevelAuthority, Map<DwfeTestLevelAuthority, Integer>> statusMap;

    if (BAD_ACCESS_TOKEN == resourceAccessingType)
      statusMap = AUTHORITY_to_AUTHORITY_STATUS_BAD_ACCESS_TOKEN;
    else
      statusMap = AUTHORITY_to_AUTHORITY_STATUS;

    VARS_FOR_AUTH_TESTS.RESOURCE_AUTHORITY_reqDATA().forEach((resource, next) -> {

      try
      {
        TimeUnit.MILLISECONDS.sleep(70);
      }
      catch (InterruptedException ignored)
      {
      }

      var next1 = next.entrySet().iterator().next();
      var next2 = next1.getValue().entrySet().iterator().next();

      var level = next1.getKey();
      var method = next2.getKey();
      var map = next2.getValue();
      var expectedStatus = statusMap.get(authorityLevel).get(level);

      if (GET == method)
        responseAfterGETrequest(resource, access_token, expectedStatus);
      else
        responseAfterPOSTrequest(resource, access_token, map, expectedStatus);
    });
  }

  public void signOutProcess(DwfeTestAuth auth, Integer expectedStatus)
  {
    log.info("sign out");

    if (expectedStatus == null)
      expectedStatus = auth.client.clientname.equals(propDwfeTest.getOauth2ClientTrusted().getId()) ? 200 : 401;

    var body = responseAfterGETrequest(
            propDwfe.getService().getGatewayUrl()
                    + propDwfe.getService().getNevis().getGatewayPath()
                    + propDwfeTest.getResource().getSignOut(),
            auth.access_token,
            expectedStatus
    );

    if (expectedStatus == 200)
    {
      assertNotEquals(null, body);
      assertEquals(true, body.get("success"));
    }
  }

  public void check(String url, RequestMethod method, String access_token, List<DwfeTestChecker> checkers)
  {
    checkers.forEach(checker -> {
      Map<String, Object> body;
      if (GET == method)
        body = responseAfterGETrequest(url, access_token, checker.expectedStatus);
      else
        body = responseAfterPOSTrequest(url, access_token, checker.requestMap, checker.expectedStatus);

      assertEquals(checker.expectedResult, body.get("success"));

      if (checker.expectedError != null)
        assertEquals(checker.expectedError, ((List<String>) body.get("error-codes")).get(0));

      if (checker.expectedResponseMap != null)
      {
        log.info("= expected: " + getJsonFromObj(checker.expectedResponseMap));


        var expectedMap = checker.expectedResponseMap;
        var actualMap = (Map<String, Object>) body.get("data");

        if (actualMap.containsKey("createdOn"))
          actualMap.put("createdOn", "date");
        if (actualMap.containsKey("updatedOn"))
          actualMap.put("updatedOn", "date");

        try
        {
          var expected = new ConcurrentSkipListMap(expectedMap);
          var actual = new ConcurrentSkipListMap(actualMap);
          assertEquals(expected.toString(), actual.toString());
        }
        catch (Throwable throwable)
        {
          assertEquals(mapToStr(expectedMap), mapToStr(actualMap));
        }
      }
    });
  }

  private String mapToStr(Map<String, Object> map)
  {
    var set = new HashSet<String>();
    map.forEach((key, value) -> {
      set.add(key + (value == null ? "null" : value.toString()));
    });
    return set.toString();
  }


  //-------------------------------------------------------
  // Common Tests
  //

  public void test_actuator()
  {
    var resource_actuator = propDwfe.getResource().getActuator();
    var resource_health = "/health";

    var url_actuator = propDwfe.getApiRoot() + resource_actuator;
    var url_health = propDwfe.getApiRoot() + resource_actuator + resource_health;

    var resp = responseAfterGETrequest(url_actuator, null, 200);
    assertEquals(1, resp.size());
    assertNotNull(resp.get("_links"));

    resp = responseAfterGETrequest(url_health, null, 200);
    assertEquals("UP", resp.get("status"));
  }

  //-------------------------------------------------------
  // Other
  //

  public static void pleaseWait(long timeToWait, Logger log)
  {
    try
    {
      log.info("::> please wait {} seconds...", timeToWait);
      TimeUnit.SECONDS.sleep(timeToWait);
      log.info("::> continue execution after waiting");
    }
    catch (InterruptedException ignored)
    {
    }
  }

  public static Resource getTestFile() throws IOException
  {
    Path testFile = Files.createTempFile("test-file-", ".txt");
    Files.write(testFile, "This is a test file".getBytes());
    return new FileSystemResource(testFile.toFile());
  }
}
