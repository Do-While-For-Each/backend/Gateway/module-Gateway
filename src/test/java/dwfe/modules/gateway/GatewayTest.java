package dwfe.modules.gateway;

import dwfe.test.DwfeTestUtil;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static dwfe.test.DwfeTestUtil.pleaseWait;

//
// == https://spring.io/guides/gs/testing-web/
//

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT  // == https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html#boot-features-testing-spring-boot-applications
)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GatewayTest
{
  private static final Logger log = LoggerFactory.getLogger(GatewayTest.class);

  @Autowired
  private DwfeTestUtil utilTest;


  //-------------------------------------------------------
  // ACTUATOR
  //

  @Test
  public void _00_01_actuator()
  {
    pleaseWait(65, log);
    utilTest.test_actuator();
  }
}
